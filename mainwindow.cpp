﻿#include "mainwindow.h"
#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QString>
#include <QStringList>
#include <QListWidgetItem>
#include <QFile>
#include <QFileDialog>

#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{

    display = new QTimeEdit(this);
    buttonStart = new QPushButton("Start", this);
    buttonFile = new QPushButton("File",this);
    hourList = new QListWidget(this);

    mainLayout = new QGridLayout(this);

    mainLayout->addWidget(display, 0, 0, 1, 1);
    mainLayout->addWidget(hourList, 2, 0, 1, 2);
    mainLayout->addWidget(buttonStart, 0, 1, 1, 1);
    mainLayout->addWidget(buttonFile, 1, 1, 1, 1);
    setLayout(mainLayout);

    connect(buttonStart, &QPushButton::clicked, this, &MainWindow::storeAlarm);
    connect(buttonFile, &QPushButton::clicked, this, &MainWindow::fromFile);

    setWindowTitle(tr("HodAlarm"));
}

void MainWindow::alarm()
{
    //buttonStart->setText("Plim");
    for(int i = 0; i < hourList->count(); i++){
        qDebug() << i << hourList->count();
        auto item = hourList->itemAt(0, i);
        qDebug() << item;
        auto *itemTimer = item->data(Qt::UserRole).value<QTimer*>();
        qDebug() << itemTimer;
        auto *timerTrigger = qobject_cast<QTimer*>(sender());
        qDebug() << timerTrigger;
        if(itemTimer == timerTrigger){
            item->setBackgroundColor(QColor(Qt::gray));
        }
    }
}

int MainWindow::anotherDay(int timeInMs)
{
    if (timeInMs <= 0){
        timeInMs += 86400000;
    }
    return timeInMs;
}

void MainWindow::createAlarm(QTime recTime)
{
    QTimer *timer = new QTimer(this);
    timer->setSingleShot(true);
    connect(timer, &QTimer::timeout, this, &MainWindow::alarm);
    int timeInMs = QTime::currentTime().msecsTo(recTime);
    timer->setInterval(anotherDay(timeInMs));
    timer->start();
    auto hourItem = new QListWidgetItem(recTime.toString());
    hourItem->setData(Qt::UserRole,QVariant::fromValue<QTimer*>(timer));
    hourList->addItem(hourItem);
}

QTime MainWindow::rawToTime(QString rawTime)
{
    const QStringList list = rawTime.split(":");
    int h = list.at(0).toInt();
    int m = list.at(1).toInt();
    QTime time = QTime(h, m, 0, 500);
    return time;
}

void MainWindow::fromFile()
{
    std::vector<QTime> vTimes;
    QString filename = QFileDialog::getOpenFileName(this, "Open File", "C:\\", "Text Files (*.txt)");
    QFile txt(filename);
    if (!txt.open(QFile::ReadOnly)){
        qDebug() << "Couldn't open file.";
    }
    QTextStream rawTimes (&txt);
    while (!rawTimes.atEnd()){
        vTimes.push_back(MainWindow::rawToTime(rawTimes.readLine()));
    }
    txt.close();

    while (!vTimes.empty()){
        createAlarm(vTimes.back());
        vTimes.pop_back();
    }
}

void MainWindow::storeAlarm()
{
    QTime time = MainWindow::rawToTime(display->text());
    MainWindow::createAlarm(time);
}
