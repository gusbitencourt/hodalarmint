#define MAINWINDOW_H
#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QListWidget>
#include <QTimeEdit>

class MainWindow:public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

private:
    QGridLayout *mainLayout;
    QTimeEdit *display;
    QPushButton *buttonStart;
    QListWidget *hourList;
    QPushButton *buttonFile;

private slots:
    void storeAlarm();
    void alarm();
    void fromFile();
    void createAlarm(QTime recAlarm);
    int anotherDay(int timeInMs);
    QTime rawToTime(QString rawTime);
};
